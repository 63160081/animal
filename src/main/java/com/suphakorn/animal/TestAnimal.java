/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.animal;

/**
 *
 * @author exhau
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        
        System.out.println("h1 is animal? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal? " + (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is land animal? " + (a1 instanceof Animal));
        System.out.println("a1 is reptile animal? " + (a1 instanceof Reptile));
    }
    
}
